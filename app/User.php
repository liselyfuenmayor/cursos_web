<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Carbon\Carbon;

class User extends Authenticatable
{

    // Relaciones del modelo

     public function roles()
    {
        return $this->belongsTo(Roles::class); //Un curso pertenece a una categoria 
    } 

        public function Cursos()
    {
        return $this->hasMany(Curso::class); //Un curso tiene muchos modulos 
    }

        public function ModuloAsesor()
    {
        return $this->hasMany(Modulos::class); //Un curso tiene muchos modulos 
    }

        public function Notas()
    {
        return $this->hasMany(Notas::class); //Un curso tiene muchos modulos 
    }

         public function certificados()
    {
        return $this->hasMany(Certificado::class); //Un curso tiene muchos certificados 
    }

      public function CursosUser()
    {
        return $this->hasMany(CursoUser::class); //Un curso tiene muchos modulos 
    }

     public function RedesUsuarios()
    {
        return $this->HasMany(RedesUsuarios::class); //Un curso pertenece a una categoria 
    } 

    // Fin de las relaciones 

   
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','user_roles_id','ci'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function getImageUrlAttribute()
    {
        
        $image= $this->image; 

        if($image){
            $image= '/images/img-perfil/'.$image; 
           return $image; //url = Accesor definido en el modelo ProductImage
        
               }
        //default
        return '/images/default.jpg';
    }


     public function getAgeAttribute()
    {
               
        $fecha= $this->birth;
        $edad= Carbon::parse($fecha)->age;

        return $edad; 
    }


   
}
