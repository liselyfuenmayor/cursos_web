<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Redes extends Model
{

	/*Relaciones del Modelo*/
 public function RedesUsuarios()
    {
        return $this->HasMany(RedesUsuarios::class); //Un curso pertenece a una categoria 
    } 
    /*Fin de las relaciones*/


/*Lo agregue como referencia para el futuro, cuando se cree el maestro de redes, por el momento debe quedarse aquí*/

      public function getImageUrlAttribute()
    {
        
        $image= $this->button; 

        if($image){
            $image= '/images/img-red/'.$image; 
           return $image; //url = Accesor definido en el modelo ProductImage
        
               }
        //default
        return '/images/default.jpg';
    }
}
