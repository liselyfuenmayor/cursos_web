<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Videos extends Model
{
	// Relaciones del modelo 

      public function modulo()
    {
        return $this->belongsTo(Modulos::class); //Un curso pertenece a una categoria 
    }

     // Fin de las relaciones 
}
