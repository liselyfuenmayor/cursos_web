<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notas extends Model
{
    // Relaciones del modelo 

      public function modulo()
    {
        return $this->belongsTo(Modulos::class); //Un curso pertenece a una categoria 
    }

     public function estudiante()
    {
        return $this->belongsTo(User::class); //Un curso pertenece a una categoria 
    }

     public function certificado()
    {
        return $this->belongsTo(CertificadoDet::class); //Un curso pertenece a una categoria 
    }
    // Fin de las relaciones 
}
