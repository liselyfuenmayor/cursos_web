<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Curso;
use App\Category;


class TestController extends Controller
{
    public function welcome(){

        $categories = Category::has('cursos')->paginate(9);
        $cursos = Curso::paginate(9);
        return view('welcome')->with(compact('cursos', 'categories'));
     
        
    }
}
