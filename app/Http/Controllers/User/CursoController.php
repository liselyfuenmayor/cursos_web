<?php

namespace App\Http\Controllers\User;

use Mail;
use App\Mail\CursosRegister;
use App\Curso;
use App\Modulos;
use App\Videos;
use App\User; 
use App\CursoUser;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CursoController extends Controller
{
    
     

      public function showlist()
    {
        $id = auth()->user()->id; 
        $curso = CursoUser::where('user_id',$id)->where('aceptado',1)->get();

        return view('user.cursos.list')->with(compact('curso'));
    }

    public function register (Request $request, $id) {

    	$CursoUser = new CursoUser();
        $CursoUser->user_id = auth()->user()->id; 
        $CursoUser->curso_id = $id;
        $CursoUser->fechainscri = now();
        $CursoUser->aceptado = 0;
        $CursoUser->save();


        $Curso = Curso::find($id); 
        $User = auth()->user();
        $Reason = $request->input('why');
        $Ntarjeta = $request->input('Ntarjeta');
        $Nbtarjeta = $request->input('Nbtarjeta');
    	$admins = User::where('roles_id', 1)->get();

        Mail::to($admins)->send(new CursosRegister ($User, $Curso , $Ntarjeta,$Nbtarjeta, $Reason ));
            
        $notification = 'Hemos enviado tu solicitud, recibiras noticias de nosotros pronto!';
        return back()->with(compact('notification'));


    }

    public function detail (Request $request, $id){

        $Curso   = Curso::find($id); 
        $Modulos = Modulos::where('curso_id','=',$id)->paginate(2);
        $Videos  = Videos::where('modulos_id',1) -> get(); 


    $view = View('user.cursos.video')->with(compact('Modulos', 'Curso','Videos'));

        if($request->ajax()){

            $id2 = $request->input("id");
            $video  = Videos::where('modulos_id',$id2)->get(); 
            $view = View('user.cursos.video')->with('Videos',$video);
            $sections = $view->renderSections();
            return Response::json($sections['contentPanel']); 
        }

        else return $view;



    }

    
}
