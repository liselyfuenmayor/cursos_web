<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Redes;
use Hash;


class UserController extends Controller
{
    
    public function index()
    {

        $userauth = auth()->user()->id;
        $user = User::find($userauth);
        $redes = Redes::all(); 

       
    
        return view('User.index')->with(compact('user' , 'redes'));
    }

  
   public function image()
    {

        $userauth = auth()->user()->id;
        $user = User::find($userauth);
        
    
        return view('User.Image.index')->with(compact('user'));
    }


    public function update(Request $request, $id)
    {

        //validar
        $messages = [
            'name.required' => 'Es necesario ingresar un nombre .',
            'name.max' => 'El nombre debe tener como máximo 255 caracteres.',
            'last.required' => 'Es necesario ingresar un apellido .',
            'last.max' => 'El Apellido debe tener como máximo 255 caracteres.',
            'tel.max' =>  'El telefono debe tener maximo 13 caracteres.',
            'gender.required' => 'Debe seleccionar su sexo',          
            'email.required' => 'El campo E-mail es requerido.',
            'email.email' => 'El formato del correo electrónico no es correcto.',
            'email.max' => 'El correo electrónico debe tener como máximo 255 caracteres.',
            'email.unique' => 'El correo electrónico ya se encuentra registrado.',
           
        ];
        
        $rules = [
            'name' => 'required|string|max:255',
            'last'  => 'required|string|max:255',
            'gender' => 'required',
            'tel'  => 'max:13',
            'email' => 'required|string|email|max:255|unique:users,email,'.$id,

            
        ];
        
        $this->validate($request, $rules, $messages );//validar

        $user = User::find($id);
        $user->name = $request->input('name');
        $user->lastname = $request->input('last');
        $user->phone = $request->input('tel');
        $user->birth = $request->input('date');
        $user->email = $request->input('email');
        $user->edad = $user->Age; 
        $user->gender = $request->input('gender');
        $user->ci = $request->input('ci');
        $user->update(); //EJECUTA Update SOBRE LA TABLA DE usuarios

        $notification = "Se han actualizado los datos del cliente satisfactoriamente.";
        return back()->with(compact('notification'));
       
    }

     public function storedpic(Request $request)
    {
         $userauth = auth()->user()->id;
         $user = User::find($userauth);
         $user->description = $request->input('description');  
         $user->update(); //EJECUTA Update SOBRE LA TABLA DE usuarios

        if ($request->file('foto') != null ) {

                      //Guardar la img en nuestro proyecto
                $file = $request->file('foto');
                $path = public_path() . '/images/img-perfil';
                $filename = uniqid() . $file->getClientOriginalName();
                $moved = $file->move($path, $filename);
                
                //Crear 1 registro en la tabla product_images
                if ($moved) {

                $userauth = auth()->user()->id;
                $user = User::find($userauth);
                $user->image = $filename;
                $user->update(); //EJECUTA Update SOBRE LA TABLA DE usuarios
               

                }
                else{

                        $error = "No se ha podido actualizar la foto ";
                        return back()->with(compact('alert'));
                     

                }

                $notification = "Se han actualizado los datos satisfactoriamente.";
                return back()->with(compact('notification'));
            

        }

        else{
                if( $request->input('description') != null ){

                         $alert = "Se actualizo la descripción, no ha actualizado la foto ";
                        return back()->with(compact('alert'));
                    }

                    else{

                         $alert = "No ha realizado cambios ";
                         return back()->with(compact('alert'));
                    }

                
            
            }
      
        
        return back();
    }




    public function ChangePassword_index()
    {
        return view('User.changePassword_index');
    }

    public function ChangePassword_store(Request $request)
    {
        //validar
        $messages = [
            'password.required' => 'El campo de contraseña actual es requerido.',
            'newpassword.required' => 'Por favor ingrese la nueva contraseña.',
            'newpassword.min' => 'La nueva contraseña debe tener al menos 6 caracteres.',
            'password_confirmation.required' => 'Por favor ingrese la confirmación de la contraseña.',
            'password_confirmation.same' => 'Las contraseñas no son iguales, por favor verifique.'
            
        ];
        
        $rules = [
            'password' => 'required',
            'newpassword' => 'required|string|min:6',
            'password_confirmation' => 'required|same:newpassword'
            
        ];
        
        $this->validate($request, $rules, $messages );//validar

        $password = auth()->user()->password;

        $password_actual = $request->input('password');
        $newpassword = $request->input('newpassword');

       if ( Hash::check($password_actual, $password)) { //Si la contraseña ingresada en el campo password es igual a la contraseña en la base de datos

                if ( Hash::check($newpassword, $password) ){

                $notification = "Ingrese una nueva contraseña distinta a la anterior.";
                return back()->with(compact('notification'));
                }

               else
               {
                    $user = auth()->user();
                    $user->password = bcrypt($newpassword);
                    $user->update();

                    $notification = "Contraseña cambiada exitosamente.";
                    return back()->with(compact('notification'));
               }
        }       
        else
        {
            $notification = "La contraseña ingresada como actual no corresponde a la registrada en la base de datos, por favor verifique.";
            return back()->with(compact('notification'));
        } 

       

       
    }
}
