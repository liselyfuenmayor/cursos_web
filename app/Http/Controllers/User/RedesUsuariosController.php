<?php

namespace App\Http\Controllers;

use App\RedesUsuarios;
use App\User;
use App\Redes;
use Illuminate\Http\Request;

class RedesUsuariosController extends Controller
{


    public function show (Request $request) {

        $id = $request->input("id");
        $userauth = auth()->user()->id;
        $user = User::find($userauth);
        $red = Redes::find($id); 
        $datosredes = RedesUsuarios::where('user_id', '=', $user->id)
                       ->where('red_id', '=',  $id)
                       ->get();
         

       $redphoto = '/images/img-red/'.$red['button'] ;      
       foreach ($datosredes as $datos) {

            $link = $datos->link;  
            $idred = $datos->id;         
        }   

              

        if (!empty($link)){ return response()->json(['success'=>  $link,'redname'=> $red['nombre'], 'id'=> $idred ,'redfoto' => $redphoto]); }

        else{ return response()->json(['success'=> $red['link'],'redname'=> $red['nombre'], 'id'=> $id , 'redfoto' => $redphoto]);}
        
    }


    public function update (Request $request){

      $id2 = $request->input("id");
      $link2 = $request->input("link");
      $userauth = auth()->user()->id;
      $user = User::find($userauth);

     $datosredes2 = RedesUsuarios::where('user_id', '=', $user->id)
                       ->where('red_id', '=',  $id2)
                       ->get();

     foreach ($datosredes2 as $datos) {

            $link3 = $datos->link;  
                   
    }   
     

      if (!empty($link3)) {


       $redes= RedesUsuarios::find($id2);
       $redes->link = $link2;
       $redes->save(); 

        


      }

      else {

        $redes = new RedesUsuarios();
        $redes->link = $link2;
        $redes->user_id = $user->id; 
        $redes->red_id = $id2; 
        $redes->save(); //EJECUTA INSERT SOBRE LA TABLA DE REDES

        

      }

       return response()->json(['success'=>'Datos Actualizados con exito' ]); 

    }


    
}
