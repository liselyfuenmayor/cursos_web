<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CursoCatController extends Controller
{
      public function show($id)
    {
        $categories = Category::find($id);
        $cursos = $categories->cursos()->paginate(10);
        return view('categoria.show')->with(compact('categories', 'cursos'));
    }

}
