<?php

namespace App\Http\Controllers;
use App\User;
use App\Curso;
use App\CursoUser;
use App\Modulos;
use App\Videos;


use Illuminate\Http\Request;

class CursoController extends Controller
{
    
     public function show($id)
    {
        $curso = Curso::find($id);
        $categorias = $curso->category;
        $userauth = auth()->user()->id;
        $registrado = CursoUser::where('user_id', '=', $userauth)
                       ->where('curso_id', '=',  $id)                 
                       ->get();


       foreach ($registrado as $datos) {
            $registro = $datos->id;  
            $aceptado = $datos->aceptado;               
        }  

         if (empty($registro)){ $registrado = 1; } 
         
         else{  if ($aceptado == 1) { $registrado = 2;  } 
                else{ $registrado = 0; } 
         }

        $Modulos = Modulos::where('curso_id','=',$id)->get();
       
        
        return view('curso.show')->with(compact('curso','categorias','registrado','Modulos'));
    }


  
}
