<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Certificado extends Model
{
    // Relaciones del modelo 

      public function estudiante()
    {
        return $this->belongsTo(User::class); //Un curso pertenece a una categoria 
    }

       

     public function curso()
    {
        return $this->belongsTo(Curso::class); //Un curso pertenece a una categoria 
    }

      public function detalles()
    {
        return $this->hasMany(CertificadoDet::class); //Un curso tiene muchos modulos 
    }


    // Fin de las relaciones 
}
