<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modulos extends Model
{
    // Relaciones del modelo 

      public function curso()
    {
        return $this->belongsTo(Curso::class); //Un curso pertenece a una categoria 
    }

     public function asesor()
    {
        return $this->belongsTo(User::class); //Un curso pertenece a una categoria 
    }

        public function videos()
    {
        return $this->hasMany(Videos::class); //Un curso tiene muchos modulos 
    }

          public function Notas()
    {
        return $this->hasMany(Notas::class); //Un curso tiene muchos modulos 
    }

        public function certificado()
    {
        return $this->hasMany(CertificadoDet::class); //Un curso tiene muchos modulos 
    }
    // Fin de las relaciones 
}
