<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RedesUsuarios extends Model
{
        public function User()
    {
        return $this->belongsTo(User::class); //Un curso pertenece a una categoria 
    } 

       public function Redes()
    {
        return $this->belongsTo(Redes::class); //Un curso pertenece a una categoria 
    }  
}
