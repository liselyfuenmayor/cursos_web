<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CursoUser extends Model
{
	//Relaciones del modelo

       public function user()
    {
        return $this->belongsTo(User::class); //Un curso pertenece a una categoria 
    }

          public function Curso()
    {
        return $this->belongsTo(Curso::class); //Un curso tiene muchos modulos 
    }

    // fin de las relaciones 

}
