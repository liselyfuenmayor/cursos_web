<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
         public function cursos()
    {
        return $this->hasMany(Curso::class); //Una categoria tiene muchos cursos 
    }
}
