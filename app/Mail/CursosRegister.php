<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Curso;
use App\User;


class CursosRegister extends Mailable
{
    use Queueable, SerializesModels;
    
    public $User;
    public $Curso;
    public $Ntarjeta;
    public $Nbtarjeta;
    public $Reason;
    

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $User, Curso $Curso,  $Ntarjeta ,  $Nbtarjeta, $Reason )
    {
        $this->User = $User;
        $this->Curso = $Curso;
        $this->Ntarjeta = $Ntarjeta;
        $this->Nbtarjeta = $Nbtarjeta;
        $this->Reason = $Reason; 

       
        
    
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->view('emails.register')
                        ->subject('Has recibido una solicitud de registro!');
    }
}
