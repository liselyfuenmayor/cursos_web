<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
 

class 	Curso extends Model
{

	// Relaciones del modelo 

    // $curso->category
    public function category()
    {
        return $this->belongsTo(Category::class); //Un producto pertenece a una categoria
    }

        public function modulos()
    {
        return $this->hasMany(Modulos::class); //Un curso tiene muchos modulos 
    }

         public function certificados()
    {
        return $this->hasMany(Certificado::class); //Un curso tiene muchos certificados 
    }

     public function User()
    {
        return $this->belongsTo(User::class); //Un curso pertenece a una categoria 
    }

    public function certificado()
    {
        return $this->hasMany(Certificado::class); //Un curso tiene muchos modulos 
    }

     public function CursosUser()
    {
        return $this->hasMany(CursoUser::class); //Un curso tiene muchos modulos 
    }



    // Fin de las relaciones 
}
