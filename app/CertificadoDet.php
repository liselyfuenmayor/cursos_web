<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CertificadoDet extends Model
{
// Relaciones del modelo 

      public function Modulo()
    {
        return $this->belongsTo(Modulo::class); //Un curso pertenece a una categoria 
    }

       

     public function certificado()
    {
        return $this->belongsTo(Certificado::class); //Un curso pertenece a una categoria 
    }

      public function detalles()
    {
        return $this->hasMany(Notas::class); //Un curso tiene muchos modulos 
    }


    // Fin de las relaciones 
}
