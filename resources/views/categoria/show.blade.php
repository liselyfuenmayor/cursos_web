@extends('layouts.app')

@section('title', 'App Shop | Dashboard')

@section('body-class', 'profile-page')

@section('styles')
    <style>
        .team{
            padding-bottom: 50px;
        }
        
        .team .row .col-md-4{
            margin-bottom: 5em;
        }
        
        .row{
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            flex-wrap: wrap;
        }
        
        .row > [class*='col-'] {
            display: flex;
            flex-direction: column;
        }
    </style>
@endsection

@section('content')

<div class="header header-filter" style="background-image: url('/img/examples/city.jpg');"></div>

<div class="main main-raised">
			<div class="profile-content">
	            <div class="container">
	                <div class="row">
	                    <div class="profile">
	                        <div class="avatar">
	                            <img src="{{ $categories->imgUrl }}" alt="Imagen representativa de la categoría {{ $categories->titulo }}" class="img-circle img-responsive img-raised">
	                        </div>
	                        <div class="name">
	                            <h3 class="title">{{ $categories->titulo }}</h3>
	                        </div>
                            
                            @if (session('notification'))
                                <div class="alert alert-success">
                                    {{ session('notification') }}
                                </div>
                            @endif
	                    </div>
	                </div>
	                <div class="description text-center">
                        <p>{{ $categories->descripcion }}</p>
	                </div>
                    
                    <div class="team text-center">
						<div class="row">
                        @foreach ($cursos as $curso)    
                          <div class="col-md-4">
                                <div class="team-player">
                                    <img src="{{ $curso->urlImg }}" alt="Thumbnail Image" class="img-raised img-circle">
                                    <h4 class="title">
                                        <a href="{{ url('/curso/'.$curso->id) }}">{{ $curso->titulo }} </a>
                                      
                                    </h4>
                                    <p class="description">
                                        {{ $curso->descripcion }}
                                    </p>
                                </div>  
                          </div>
                        @endforeach    
						</div>
                        
                        <div class="text-center">
                            {{ $cursos->links() }}
                        </div>
					</div>
					

	            </div>
	        </div>
		</div>


    

@include('includes.footer')
@endsection



