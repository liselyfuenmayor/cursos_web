@extends('layouts.app')

@section('title', 'Cursos Web')

@section('body-class', 'landing-page')




@section('content')

<div class="header header-filter" style="background-image: url('https://images.unsplash.com/photo-1423655156442-ccc11daa4e99?crop=entropy&dpr=2&fit=crop&fm=jpg&h=750&ixjsv=2.1.0&ixlib=rb-0.3.5&q=50&w=1450');">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h1 class="title">Bienvenidos a Cursos Web </h1>
                        <h4>Una pagina dedicada al  emprendimiento personal, aprende con nosotros</h4>
                        <br />
                        <a href="https://www.youtube.com/watch?v=dQw4w9WgXcQ" class="btn btn-danger btn-raised btn-lg">
                            <i class="fa fa-play"></i> ¿Como Funciona?
                        </a>
                    </div>
                </div>
            </div>
        </div>

<div class="main main-raised">
            <div class="container">
                <div class="section text-center section-landing">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <h2 class="title">¿Por que ser parte de cursos web?</h2>
                            <h5 class="description">Somos una comunidad dedicada a aprender, donde todos contribuimos,  puedes aprender y enseñar. que esperas!</h5>
                        </div>
                    </div>

                    <div class="features">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="info">
                                    <div class="icon icon-primary">
                                        <i class="material-icons">chat</i>
                                    </div>
                                    <h4 class="info-title">Atendemos tus dudas</h4>
                                    <p>Atendemos rápidamente cualquier consulta que tengas. No estas sólo, ya que siempre estamos atentos a tus inquietudes</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="info">
                                    <div class="icon icon-success">
                                        <i class="material-icons">verified_user</i>
                                    </div>
                                    <h4 class="info-title">Contenido Seguro</h4>
                                    <p>Contenido 100% seguro libre de virus y publicidad.</p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="info">
                                    <div class="icon icon-danger">
                                        <i class="material-icons">fingerprint</i>
                                    </div>
                                    <h4 class="info-title">Información privada</h4>
                                    <p>Puedes visualizar y subir cursos bajo tu alias sin tener que compartir tu perfil, protegemos tu información nos importa el bienestar de toda nuestra comunidad.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="section text-center">
                    <h2 class="title">Nuestros Cursos</h2>
                    
               <!--      <form class="form-inline" method="get" action="{{ url('/search') }}">
                        <input type="text" placeholder="Que producto buscas?" class="form-control" name="query" id="search">
                        
                        <button type="submit" class="btn btn-primary btn-just-icon">
                            <i class="material-icons">search</i>
                        </button>
                    </form> -->
                    
                    <div class="team">
                        <div class="row">
                                      @foreach ($cursos as $curso)    
                                            <div class="col-md-4">
                                                  <div class="team-player">
                                                      <img src="{{ $curso->urlImg }}" alt="Thumbnail Image" class="img-raised img-circle">
                                                      <h4 class="title">
                                                          <a href="{{ url('/curso/'.$curso->id) }}">{{ $curso->titulo }} </a>
                                                          
                                                      </h4> 
                                                      <small >{{ $curso->category->titulo }}</small>
                                                   
                                                  </div>  
                                            </div>
                                        @endforeach 
                        </div>
                        
                        <div class="text-center">
                           {{ $cursos->links() }}
                        </div>
                    </div>
                </div>
                
                <div class="section text-center">
                    <h2 class="title">Nuestras Categorías</h2>
                    
                    <div class="team">
                        <div class="row">

                                
                                @foreach ($categories as $CursoCat)    
                                  <div class="col-md-4">
                                        <div class="team-player">
                                            
                                            <img src="{{ $CursoCat->urlImg }}" alt="Thumbnail Image" class="img-raised img-circle">
                                            <h4 class="title">
                                                <a href="{{ url('/categoria/'.$CursoCat->id) }}">{{ $CursoCat->titulo }} </a>
                                                <br />
                                                <small class="text-muted">{{ $CursoCat->descripcion }}</small>
                                            </h4>
                                            
                                        </div>  
                                  </div>
                                @endforeach   
                       </div>
                        
                        <div class="text-center"> 
                            {{ $categories->links() }}
                        </div>
                    </div>
                </div>


                <div class="section landing-section">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <h2 class="text-center title">¿Aún no te has registrado?</h2>
                            <h4 class="text-center description">Regístrate ingresando tus datos básicos, y podrás acceder a contenido exclusivo, Si aun no te decides a ser parte de la comunidad, de todas formas, con tu cuenta de usuario podrás hacer todas tus consultas sin compromiso.</h4>
                            <form class="contact-form">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Nombre</label>
                                            <input type="email" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Correo Electrónico</label>
                                            <input type="email" class="form-control">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group label-floating">
                                    <label class="control-label">Tu Mensaje</label>
                                    <textarea class="form-control" rows="4"></textarea>
                                </div>

                                <div class="row">
                                    <div class="col-md-4 col-md-offset-4 text-center">
                                        <button class="btn btn-primary btn-raised">
                                            Enviar
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>

        </div>

@include('includes.footer')
@endsection