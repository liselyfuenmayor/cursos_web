@extends('layouts.dash')

@section('title', 'Editar foto de perfil')

@section('li2-class', 'active')

@section('styles')

<link href="{{ asset('css/estilos.css') }}" rel="stylesheet" />

<style>
    
.card-profile .card-avatar {
    margin: 10px auto 10px;
    height: 200px;
    width: 200px;
}

.card-profile{
    margin-top: 30px;
    text-align: left;
}



</style>

@endsection

@section('content')

 <div class="container-fluid">
    <div class="row">
        <div class="col-md-8">
            <div class="card card-profile">

                                 @if (session('notification'))
                                    <div class="alert alert-success">
                                        {{ session('notification') }}
                                    </div>
                                @endif

                                @if (session('alert'))
                                    <div class="alert alert-info">
                                        {{ session('alert') }}
                                    </div>
                                @endif

                                 @if (session('error'))
                                    <div class="alert alert-danger">
                                        {{ session('error') }}
                                    </div>
                                @endif

                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                   <div class="card-header" data-background-color="purple">
                    <h4 class="title">Editar imagen de perfil </h4>
                    <p class="category">"{{ $user->name }}"</p>
                   </div>
                   <div class="card-content">

                                <div class="card-avatar">
                                 
                                    <img class="img"  src="{{ $user->image_url }}" />
                                   
                                </div>


                            <form method="post" action="" enctype="multipart/form-data">

                               
                                {{ csrf_field() }}

                                <label>Actualizar foto de perfil</label>

                                <input type="file" name="foto" > 



                                   <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                   
                                                    <div class="form-group label-floating">
                                                        <label class="control-label"> Sobre mí</label>
                                                        <textarea class="form-control" rows="5" name="description" id="decription">  {{$user->description}} 
 </textarea>
                                                    </div>
                                                </div>
                                            </div>
                                   </div>

                                    <div class="footer text-center">

                                    <button class="btn btn-primary btn-round">Actualizar información</button>
                                <a href="{{ url('User/cuenta') }}" class="btn btn-default btn-round">Volver al perfil</a>

                                   </div>

                            </form> 
                                   
                  </div> 
           </div>
         </div>

          <!--  <div class="col-md-4">
                            <div class="card card-profile">
                                
                                <div class="content">
                                    <h6 class="category text-gray"> Datos personales</h6>
                                    <h4 class="card-title">{{ $user->name }}</h4>
                                    <p class="card-content">
                                        <p> {{ $user->email }}</p>
                                    </p>
                                    <a href="{{ url('User/cuenta/foto') }}" class="btn btn-primary btn-round">editar</a>
                                </div>
                            </div>
            </div> -->
            </div>
            </div>
@endsection