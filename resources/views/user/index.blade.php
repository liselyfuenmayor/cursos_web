@extends('layouts.dash')

@section('title', 'Mi Perfil')

@section('li2-class', 'active')

@section('styles')

<link href="{{ asset('css/estilos.css') }}" rel="stylesheet" />

@endsection

@section('content')

 <div class="container-fluid">
    <div class="row">
        <div class="col-md-8">
            <div class="card">

                 @if (session('notification'))
                    <div class="alert alert-success">
                        {{ session('notification') }}
                    </div>
                @endif

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif


                <div class="card-header" data-background-color="purple">
                    <h4 class="title">Modificar perfil</h4>
                    <p class="category">Cuentanos más sobre ti</p>
                </div>
                <div class="card-content">
                   <form class="form" method="POST" action="{{ url('/User/cuenta/'.$user->id.'/update') }}">
                     {{ csrf_field() }}
                        <div class="row">
                         
                         
                            <div class="col-md-5">
                                <div class="form-group label-floating">
                                    <label class="control-label">Nombre</label>
                                    <input type="text" class="form-control"  name="name" value="{{ $user->name }}" required autofocus>
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                </div>
                            </div>

                            <div class="col-md-1">

                                </div>

                             <div class="col-md-5">
                                <div class="form-group label-floating">
                                    <label class="control-label">Apellido</label>
                                    <input type="text" class="form-control"  name="last" value="{{ $user->lastname }}" required autofocus>
                                        @if ($errors->has('last'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('last') }}</strong>
                                            </span>
                                        @endif
                                </div>
                            </div>                

                        </div>

                        <div class="row">

                            <div class="col-md-5">
                                <div class="form-group label-floating">
                                    <label class="control-label">Ci</label>
                                     <input id="ci" type="number" class="form-control" name="ci" value="{{ $user->ci }}" placeholder="Cédula de identidad" required readonly autofocus>

                                    @if ($errors->has('ci'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('ci') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                             <div class="col-md-1">

                                </div>


                             <div class="col-md-5">
                                <div class="form-group label-floating">
                                    <label class="control-label">Email address</label>
                                     <input id="email" type="email" class="form-control" name="email" value="{{ $user->email }}" placeholder="Correo Electrónico..." required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                        </div>

                               <div class="row">
                         

                              <div class="col-md-5">
                                <div class="form-group label-floating">
                                    <label class="control-label">Telf.</label>
                                     <input id="tel" type="number" class="form-control" name="tel" value="{{ $user->phone }}"  autofocus>

                                    @if ($errors->has('phone'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('phone') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                             <div class="col-md-1">

                                </div>

                        <div class="col-md-5">
                                <div class="form-group label-floating">
                                    <label class="control-label">Fecha de nacimiento</label>
                                     <input id="tel" type="date" class="form-control" name="date" value="{{ $user->birth }}"   autofocus>

                                    @if ($errors->has('date'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('date') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                       

                        </div>

                        <div class="row">
                         

                              <div class="col-md-5">
                                <div class="form-group label-floating">
                                    <label class="control-label">Edad</label>
                                     <input id="tel" type="text" class="form-control" name="age" value="{{ $user->Age }}"   autofocus readonly>

                                    @if ($errors->has('phone'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('phone') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                             <div class="col-md-1">

                                </div>


                        <div class="col-md-5">
                                <div class="form-group label-floating">
                        <label class="control-label">Sexo</label>

                    @if ($user->gender)
                       
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="gender" value="1" checked="true">
                                            Femenino
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="gender" value="0">
                                            Masculino
                                        </label>
                                    </div>

                      
                       @else
                         <div class="radio">
                                        <label>
                                            <input type="radio" name="gender" value="1" >
                                            Femenino
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="gender" value="0" checked="true">
                                            Masculino
                                        </label>
                                    </div>

                        @endif


                                    @if ($errors->has('gender'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('gender') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>


                        </div>


            <div class="footer text-center">

                 <button class="btn btn-primary btn-round">Editar Perfil</button>
                <a href="{{ url('/home') }}" class="btn btn-default btn-round">Cancelar</a>

                
                        
                    </form>

                    </div>


               
                    </div>


            </div>

              <center>

             @foreach ($redes as $red)  
                <button class="btn btn-fab  red" id="{{ $red->id }}"   value="{{ $red->id }}" 
                  style='background:url({{ $red->ImageUrl }}); background-size:cover;' > 
                 </button>
             @endforeach 
               
                 <div class="separator"></div>

                </center>  
          
            <div class="modal fade" id='miVentana' tabindex='-1' role='dialog' aria-labellebdy='myModalLabel'
              aria-hidden='true'>
              
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                    <h2 class="text-center" id="prueba"></h2>
                    </div>

                    <div class="modal-body">

                    <div class="alert alert-success" id="alerta"
                    hidden></div>

                    <form class="form" method="POST" action="{{ url('/User/Redes/$id/update') }}">
                    {{ csrf_field() }}

                      <div class="row">
                         
                       
                              <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label >Link</label>
                                     <input id="link" type="text" class="form-control" name="link"  autofocus>
   
                                </div>
                            </div>

                        <input type="text" hidden name="id"  id="idred"> 

                     </div>

                        </form>
                     
                    </div>
                    <div class="modal-footer">
                      <button  class="btn btn-primary" data-dismiss="modal"  id="aceptar">Aceptar</button>
                      <button  class="btn btn-primary"   id="actualizar">Actualizar</button>
                    </div>
                  </div>
                  
                </div>

              </div>


        </div>
        <div class="col-md-4">
            <div class="card card-profile">
                 <div class="card-avatar">
                 
                    <img class="img"  src="{{ $user->image_url }}" />
                   
                </div>
                <div class="content">
                    
                    <h4 class="card-title">{{$user->name}}</h4>
                    <p class="tim-typo">  {{$user->roles->descripcion}} </p>
                    <p class="card-content">
                        {{$user->description}}
                    </p>
                    <a href="{{ url('User/cuenta/foto') }}" class="btn btn-primary btn-round">editar</a>
                </div>
            </div>

            <br><br><br>

            <div class="card card-profile">
                
                <div class="content">

                     <div class="card-avatar">
                 
                    <img class="img" id="redphoto"  src="/images/default.jpg" />
                   
                </div>
                    
                    <h4 class="tim-typo2" id="redname">Red</h4>
                    
                    <p class="card-content">

                            <a class="row tim-typo" id="link2" href="Mi link" target="_blank">
                                Link de mi red
                            </a>

                             <br>
                             <br>

                    </p>
                    
                </div>
            </div>


        </div>
    </div>
</div>  


    <script type="text/javascript">
    jQuery(document).ready(function(){

         $(".red").hover(function(e){

          e.preventDefault();
          
            $.ajaxSetup({
                 headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 }
            });
   
            var ruta = "{{ url('cuenta/redes') }}"; 
            var id= $(this).val();
   
            jQuery.ajax({

                 url: ruta,
                 data: {id: id } ,
                 method: 'get',
                 contentType: 'application/x-www-form-urlencoded',

                 success: function(response){
                     $link = response.success;
                     $red = response.redname; 
                     $redphoto = response.redfoto;
                  
                     $('#redname').delay("slow").html($red);
                     $('#link2').delay("slow").html($link);
                     $('#link2').delay("slow").prop("href", "http://"+$link);
                     $('#redphoto').delay("slow").prop("src",$redphoto);
                 },

                 error: function(response){

                    $response = response.error; 
                     $('#link2').html('Error: ha ocurrido un error en el servidor 500');      
                }
            });
      
        });




        jQuery('.red').click(function(e){

          e.preventDefault();
          
            $.ajaxSetup({
                 headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 }
            });
            


            var ruta = "{{ url('cuenta/redes') }}"; 
            var id= $(this).val();

           
            jQuery.ajax({

                 url: ruta,
                 data: {id: id } ,
                 method: 'get',
                 contentType: 'application/x-www-form-urlencoded',

                 success: function(response){
                     $link = response.success;
                     $red = response.redname;
                     $id = response.id; 
                  
                     $('#miVentana').modal('show'); 
                     $('#prueba').html($red);
                     $('#link').val($link);
                     $('#idred').attr("value",$id);
                 },

                 error: function(response){

                    $response = response.error;
                     $('#miVentana').modal('show'); 
                     $('#prueba').html('Error: ha ocurrido un error en el servidor 500');
                    
                   
                }
            });
            

        });


         jQuery('#actualizar').click(function(e){

          e.preventDefault();
          
            $.ajaxSetup({
                 headers: {
                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                 }
            });
            
           
            var id = $('#idred').val(); 
            var link = $('#link').val(); 
            var ruta = "{{ url('cuenta/redes/update') }}"; 
           

           
            jQuery.ajax({

                 url: ruta,
                 data: {id: id,link: link } ,
                 method: 'get',
                 contentType: 'application/x-www-form-urlencoded',

                 success: function(response){

                     $msg = response.success;
     
                     $('#alerta').show(); 
                     $('#alerta').html($msg);
                    
                 },

                 error: function(response){

                     $('#alerta').show(); 
                     $('#alerta').html('error al actualizar');
                    
                   
                }
            });
            

        });


        jQuery('#aceptar').click(function(){

                     $('#alerta').hide();
                    

        });



    });
</script>                          
					

@endsection