@extends('layouts.dash')

@section('title', 'Mis Cursos')

@section('li3-class', 'active')

@section('styles')

<link href="{{ asset('css/estilos.css') }}" rel="stylesheet" />

@endsection

@section('content')


  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Modulo', 'Nota'],
            @foreach ($notas as $nota)
              ['Modulo', {{ $nota->nota}}],
            @endforeach
        ]);

        var options = {
          title: 'Notas por módulo',
          curveType: 'function',
          legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
    </script>

     <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header" data-background-color="purple">
                                    <h4 class="title">Grafico de notas</h4>
                                    
                                </div>

                                @if (session('notification'))
                                <div class="alert alert-success">
                                    {{ session('notification') }}
                                </div>
                                @endif


                                <div id="curve_chart" style="width: 900px; height: 500px"></div>
                             
                            </div>
                        </div>
                       
                        </div>
                      
                    </div>
               

@endsection