@extends('layouts.dash')

@section('title', 'Mis Cursos')

@section('li3-class', 'active')

@section('styles')

<link href="{{ asset('css/estilos.css') }}" rel="stylesheet" />

@endsection

@section('content')

     <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header" data-background-color="purple">
                                    <h4 class="title">Mis cursos</h4>
                                    
                                </div>
                                @if (session('notification'))
                                <div class="alert alert-success">
                                    {{ session('notification') }}
                                </div>
                            @endif
                                <div class="card-content table-responsive">
                                    <table class="table">
                                        <thead class="text-primary">
                                            
                                            <th>Curso</th>
                                            <th>Fecha de inscripción</th>
                                            <th>Porcentaje culminado</th>
                                            <th>Nota</th>
                                            <th>Fecha de certificación</th>
                                        </thead>
                                        <tbody>
                                        

                                @foreach ($curso as $CursoUser)  
                                    <tr>
                                        
                                        <td>{{ $CursoUser->curso->titulo }}</td>
                                        <td>{{ $CursoUser->fechainscri }}</td> 
                                        <td>{{ $CursoUser->porcenculmin }} %</td>
                                        <td class="text-primary">{{ $CursoUser->notafinal }}</td>
                                       <td>{{ $CursoUser->fechacerti }}</td>
                                        <td> <a href="{{ url('/User/miscursos/'.$CursoUser->curso->id.'/Modulos') }}"
                                            class="btn btn-primary btn-fab btn-fab-mini btn-round"><i class="material-icons">search</i> </a>  </td>
                                    </tr>
                                @endforeach 
                                    
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                       
                        </div>
                        <div class="row">
                            
                            <a href="{{ url('/User/graficodenotas') }}"
                            class="btn btn-primary "> Acceder al grafico de notas </a>
                        </div>
                    </div>
               

@endsection