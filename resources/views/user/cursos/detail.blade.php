@extends('layouts.dash')

@section('title', 'Mis Cursos')

@section('li3-class', 'active')

@section('styles')

<link href="{{ asset('css/estilos.css') }}" rel="stylesheet" />

@endsection

@section('content')

     <div class="container-fluid">
                   <div class="row">
                    <div class="col-md-12">                                              
                        <div class="tim-typo">
                            <h2>
                             {{ $Curso->titulo }} -
                           
                            <small>   Prof. {{ $Curso->User->name }} {{ $Curso->User->lastname }}</small>
                            </h2>
                        </div>

                    </div>
                   </div>

                    <div class="row">

                        <div class="col-md-5">
                                
                                <img src="{{ $Curso->urlImg }}" height="350" width="350" class="img-circle img-responsive img-raised">
                       </div>

                        <div class="col-md-7">


                            <div class="card">
                                <div class="card-header" data-background-color="purple">
                                    <h4 class="title">Módulos del curso</h4>
                                    
                                </div>
                              

                    <div class="card-content table-responsive">

                      <table class="table">
                         <thead class="text-primary">    
                                       
                                <th>Modulo</th>
                                <th>Descripción</th>
                                                                   
                        </thead>

                         <tbody>
                           
                            @foreach ($Modulos as $modulo) 
                            <tr class="modulo" id="{{ $modulo->id }}">
                               
                                <td> {{ $modulo->titulo }} </td>
                                <td> {{ $modulo->descripcion }} </td>
                                 
                            </tr>  
                               
                        
                            @endforeach
                        </tbody>

                      </table>
                         <div class="text-center">
                           {{ $Modulos->links() }}
                            </div>
                                </div>

                            </div>
                        </div>
                       
                        </div>
                        <div class="separator"></div>

                <div class="row">

                   <!--  <p> {{ $Curso->descripcion }} </p>
 -->
                     <div class="col-md-12">
                         <div class="card-content table-responsive">

                      <table class="table">
                         <thead class="text-primary">    
                                       
                                <th>Video</th>
                                <th>Descripción</th>
                                                                   
                        </thead>

 

                        
                         <tbody id="contenido">
                             <div id="principalPanel">
                                    @section('contentPanel')
                                         
                                    @show
                            </div>
                            
                        </tbody> 
                        
                    
                      </table>
                        
                        </div>
                    </div> 

                </div>    

    </div>

  


@endsection

@section('script')


@endsection
