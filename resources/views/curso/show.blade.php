@extends('layouts.app')

@section('title', 'Cursos Web | Curso')

@section('body-class', 'profile-page')

@section('content')

<div class="header header-filter" style="background-image: url('/img/examples/city.jpg');"></div>

<div class="main main-raised">
			<div class="profile-content">
	            <div class="container">
	                <div class="row">
	                    <div class="profile">
	                        <div class="avatar">
	                            <img src="{{ $curso->urlImg }}" alt="Circle Image" class="img-circle img-responsive img-raised">
	                        </div>
	                        <div class="name">
	                            <h3 class="title">{{ $curso->titulo }}</h3>
	                            <h3 class="title">{{ $curso->precio }} $</h3>
								
	                        </div>
                            
                            @if (session('notification'))
                                <div class="alert alert-success">
                                    {{ session('notification') }}
                                </div>
                            @endif
	                    </div>
	                </div>
	                <div class="description text-center">
                        <p>{{ $curso->descripcion }}</p>
	                </div>
                    
                    <div class="text-center">
                    @if (auth()->check())   
                        <button class="btn btn-primary btn-round" data-toggle="modal" data-target="#modalInscribir">
                            <i class="material-icons">add</i> Inscribirte
                        </button>
                    @else
                        <a href="{{ url('/login?redirect_to='.url()->current()) }}" class="btn btn-primary btn-round">
                            <i class="material-icons">add</i> Inscribirte
                        </a>
                    @endif    
                    </div>
        
			      <div class="row">
			      	<div class="col-md-3"></div>
			      	<div class="col-md-6 ">
						<h6> Contenido Programatico </h6>
							<ol>

							@foreach ($Modulos as $modulo)  
							  <li >
							  	  {{ $modulo->titulo }}

							  	   <ul class="list-group">
							  	     @foreach ($modulo->videos as $videos)

							  	       <li class="list-group-item">
							  	       {{$videos->titulo}} 

							  	       @if($registrado == 2) 
							  	       
                                        <a class="btn btn-primary btn-fab btn-fab-mini btn-round" href="{{$videos->urlVideo}}" align="right">
										<i class="material-icons">play_arrow</i>
										</a>
									  

							  	       @endif
							  	       </li>
							  	     @endforeach 
							  	  </ul>	
							  </li>

							@endforeach

						    </ol>			
				    </div>
				    <div class="col-md-3"></div>
		         </div>

	            </div>
	        </div>
		</div>

<!-- Modal Core -->
<div class="modal fade" id="modalInscribir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Inscripcion</h4>
     
        @if($registrado == 1)


        <form class="form" method="POST" action="{{ url('/User/'.$curso->id.'/inscribirme') }}">
                     {{ csrf_field() }}

         <div class="col-md-10">
                    <div class="form-group label-floating">
                        <label class="control-label">Razón por la que quiero realizar el curso</label>
                        <input type="text" class="form-control"  name="why" value="" required autofocus>
                           <!--  @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif -->
                    </div>
          </div>

	           @if ($curso->precio > 0)

	            <div class="col-md-10">
	                    <div class="form-group label-floating">
	                        <label class="control-label">N° Tarjeta para el Pago</label>
	                        <input type="text" class="form-control"  name="Ntarjeta" value="" required autofocus>
	                           <!--  @if ($errors->has('name'))
	                                <span class="help-block">
	                                    <strong>{{ $errors->first('name') }}</strong>
	                                </span>
	                            @endif -->
	                    </div>

	          </div>

	            <div class="col-md-10">
	                    <div class="form-group label-floating">
	                        <label class="control-label">Nombre en la tarjeta</label>
	                        <input type="text" class="form-control"  name="Nbtarjeta" value="" required autofocus>
	                           <!--  @if ($errors->has('name'))
	                                <span class="help-block">
	                                    <strong>{{ $errors->first('name') }}</strong>
	                                </span>
	                            @endif -->
	                    </div>
	                    
	          </div>

	           @endif

           



     		
        <div class="footer text-center"> </center>

             <div class="col-md-12">
               <button type="submit" class="btn btn-primary btn-round">Enviar solicitud</button>
               <button  class="btn btn-primary btn-round" data-dismiss="modal"  id="aceptar">Cancelar</button>
          @endif

          @if($registrado == 2)

          <p> Ya te has inscrito en este curso </p>
          
		  @endif

          @if($registrado == 0)

           <p> Ya hay una solicitud en tramites no puedes registrarte </p>
      
          @endif
               
     		</div>

       </form>
             </div>



      </div>
        
     
      
    </div>
  </div>
</div>
    


@endsection



