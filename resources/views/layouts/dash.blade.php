<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name ="csrf-token" content = "{{csrf_token ()}}">
    
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="../assets/img/favicon.png" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>@yield('title', 'Dashboard')</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="{{ asset('assetsdash/css/material-dashboard.css?v=1.2.0') }}" rel="stylesheet" />
   



    <!--  CSS for Demo Purpose, don't include it in your project     -->
   <!--  <link href="../assets/css/demo.css" rel="stylesheet" /> -->
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>

       <!--   Core JS Files   -->
    <script src="{{ asset('js/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/material.min.js') }}"></script>

    @yield('styles')
</head>

<body>
    <div class="wrapper">
        <div class="sidebar" data-color="purple" data-image="../assets/img/sidebar-1.jpg">
            <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

        Tip 2: you can also add an image using data-image tag
    -->
            <div class="logo text-center" >
               
               <img src="{{ asset('images/icons/logo.svg') }}" height="50" width="50">
            </div>
            <div class="sidebar-wrapper">
                <ul class="nav">
                    <li class="@yield('li1-class')">
                        <a href="{{ url('/home') }}"> 
                            <i class="material-icons">dashboard</i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                    <li class="@yield('li2-class')">
                        <a href="{{ url('/User/cuenta') }}">
                            <i class="material-icons">person</i>
                            <p>Datos del perfil</p>
                        </a>
                    </li>

                   <!--    <li class="dropdown">
                        <a href="{{ url('/User/'.auth()->User()->id.'/miscursos') }}" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="material-icons">content_paste</i>
                            <p>Cursos</p>
                        </a>

                          <ul class="dropdown-menu">
                                    <li>
                                        <a href="#">Mike John responded to your email</a>
                                    </li>
                                    <li>
                                        <a href="#">You have 5 new tasks</a>
                                    </li>
                                    <li>
                                        <a href="#">You're now friend with Andrew</a>
                                    </li>
                                    <li>
                                        <a href="#">Another Notification</a>
                                    </li>
                                    <li>
                                        <a href="#">Another One</a>
                                    </li>
                                </ul>
                    </li> -->
                    
                    <li class="@yield('li3-class')">
                        <a href="{{ url('/User/miscursos') }}">
                            <i class="material-icons">content_paste</i>
                            <p>Cursos</p>
                        </a>
                    </li>
                 
                    <li class="@yield('li4-class')">
                        <a href="./typography.html">
                            <i class="material-icons">library_books</i>
                            <p>Certificados</p>
                        </a>
                    </li>
                    <li class="@yield('li5-class')">
                        <a href="./icons.html">
                            <i class="material-icons">bubble_chart</i>
                            <p>Calendario</p>
                        </a>
                    </li>
                 
                    <li class="@yield('li7-class')">
                        <a href="./notifications.html">
                            <i class="material-icons text-gray">notifications</i>
                            <p>Mi mundo interactivo</p>
                        </a>
                    </li>
                   
                </ul>
            </div>
        </div>
        <div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="{{ url('/') }}"> CursosWeb.com </a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">dashboard</i>
                                    <p class="hidden-lg hidden-md">Dashboard</p>
                                </a>
                            </li>
                    
                            <li class="dropdown">
                                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">person</i>

                                    <p class="hidden-lg hidden-md">Profile</p>
                                </a>
                                 <ul class="dropdown-menu">
                                     @guest
                        <li><a href="{{ route('login') }}">Ingresar</a></li>
                        <li><a href="{{ route('register') }}">Registro</a></li>
                    @else
                        <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                          {{ Auth::user()->name }} 
                        </a>      
                    
                           
                   <!--          @if (auth()->user()->admin)
                                <li>
                                    <a href="{{ url('/admin/products') }}">Gestionar Productos</a>
                                </li>
                                <li>
                                    <a href="{{ url('/admin/categories') }}">Gestionar Categorías</a>
                                </li>
                                <li>
                                    <a href="{{ url('/admin/UserAdmin') }}">Gestionar Usuarios Administradores</a>
                                </li>
                            @endif -->
                         
                           <li>
                                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    Cerrar sesión
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                           </li>
                       
                        </li>
                    @endguest
                                </ul>
                            </li>
                        </ul>
                        <form class="navbar-form navbar-right" role="search">
                            <div class="form-group  is-empty">
                                <input type="text" class="form-control" placeholder="Search">
                                <span class="material-input"></span>
                            </div>
                            <button type="submit" class="btn btn-white btn-round btn-just-icon">
                                <i class="material-icons">search</i>
                                <div class="ripple-container"></div>
                            </button>
                        </form>
                    </div>
                </div>
            </nav>
            <div class="content">
                 @yield('content')
            </div>
    
        </div>

    </div>

</body>




    <!--  Charts Plugin -->
    <script src=" {{ asset('assetsdash/js/chartist.min.js') }}"></script>
    <!--  Dynamic Elements plugin -->
    <script src="{{ asset('assetsdash/js/arrive.min.js') }}"></script>
    <!--  PerfectScrollbar Library -->
    <script src="{{ asset('assetsdash/js/perfect-scrollbar.jquery.min.js') }}"></script>
    <!--  Notifications Plugin    -->
    <script src="{{ asset('assetsdash/js/bootstrap-notify.js') }}"></script>
    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
    <!-- Material Dashboard javascript methods -->
    <script src="{{ asset('assetsdash/js/material-dashboard.js?v=1.2.0') }}"></script>
 <!--    Material Dashboard DEMO methods, don't include it in your project!
    <script src="{{ asset('assetsdash/js/demo.js"></script> -->
   <!--  <script type="text/javascript">
        $(document).ready(function() {

            // Javascript method's body can be found in assets/js/demos.js
            demo.initDashboardPageCharts();

        });
    </script> -->
@yield('scripts')

</html>