<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>


## Como instalar 


- Composer install
- Composer Update
- Crear base de datos cursos_web en mysql 
- php artisan migrate
- php artisan db:seed
- php artisan serve

Este proyecto es una version vieja recomendable utilizar php 7.2 
