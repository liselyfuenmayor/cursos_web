<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'TestController@welcome');

Route::get('/curso/{id}', 'CursoController@show'); //
Route::get('/categoria/{id}', 'CursoCatController@show');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//Editar datos usuario

Route::middleware(['auth'])->prefix('User')->namespace('User')->group(function (){
    
    Route::get('/cuenta', 'UserController@index');
    Route::post('/cuenta/{id}/update', 'UserController@update');

    Route::get('/cuenta/cambioclave', 'UserController@ChangePassword_index');
    Route::post('/cuenta/cambioclave', 'UserController@ChangePassword_store');

    Route::get('/cuenta/foto', 'UserController@image');
    Route::post('/cuenta/foto', 'UserController@storedpic');
    
    Route::get('/miscursos', 'CursoController@showlist');
    Route::get('/graficodenotas', 'GraficoController@index');
    Route::post('{id}/inscribirme', 'CursoController@register');

    Route::get('/miscursos/{id}/Modulos', 'CursoController@detail');
    Route::get('/miscursos/Modulos/Videos', 'CursoController@detail');
    

    

});

   /*Estas dos rutas deben estar dentro del prefijo user, por el momento estan aqui, pero debes recordar cambiarlas a donde van*/
   Route::get('cuenta/redes', 'RedesUsuariosController@show');
   Route::get('cuenta/redes/update', 'RedesUsuariosController@update');




