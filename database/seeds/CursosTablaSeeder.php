<?php

use Illuminate\Database\Seeder;
use App\Curso; 
use App\Category;
use App\Modulos;
use App\Videos;

class CursosTablaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = factory(Category::class, 5)->create(); 
        $categories->each(function ($c) {       
            $cursos = factory(Curso::class, 5)->make();
            $c->cursos()->saveMany($cursos);     
            
            $cursos->each(function($m) {      
                $modulos = factory(Modulos::class, 3)->make();      
                $m->modulos()->saveMany($modulos);    

                      $modulos->each(function($v) {      
                         $videos = factory(Videos::class, 3)->make();      
                           $v->videos()->saveMany($videos);    
                
                      }); 

            }); 
        });

    }
}
