<?php

use Illuminate\Database\Seeder;
use App\CursoUser;
class CursosUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
   public function run()
    {
        factory(CursoUser::class, 20)->create();
    }
}