<?php

use Illuminate\Database\Seeder;
use App\Roles;
class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	Roles::create([
            'descripcion' => 'Administrador',
            'descripcion2' => 'Personal encargado del mantenimiento de la pagina'
                
        ]);

        factory(Roles::class, 5)->create();
    }
}
