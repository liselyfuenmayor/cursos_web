<?php

use Illuminate\Database\Seeder;
use App\Redes;

class RedesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Redes::create([
            'nombre' => 'Facebook',
            'link' => 'www.facebook.com',
            'button' =>'buttonfacebook.png'
                
        ]);

         Redes::create([
            'nombre' => 'Instagram',
            'link' => 'www.instagram.com',
            'button' =>'buttoninstagram.png'
                
        ]);

          Redes::create([
            'nombre' => 'Google+',
            'link' => 'www.google.com/gmail/',
            'button' =>'buttongoogle.png'
                
        ]);

       Redes::create([
            'nombre' => 'LinkedIn',
            'link' => 've.linkedin.com',
            'button' =>'buttonllinkedind.png'
                
        ]);

           Redes::create([
            'nombre' => 'GitHub',
            'link' => 'www.github.com',
            'button' =>'buttongithub.png'
                
        ]);
    }
}
