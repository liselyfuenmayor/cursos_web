<?php

use Illuminate\Database\Seeder;
use App\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	 

           User::create([
            'name' => 'Lisely',
            'email' => 'LiselyCarolina@gmail.com',
            'password' => bcrypt('123456'), //Helpers para encriptar
            'ci'  => 22168099,
            'roles_id' => 1
        ]);

        factory(User::class, 5)->create();
    }
}
