<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRedesUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('redes_usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('link');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('red_id');
            $table->timestamps();

             $table->foreign('user_id')->references('id')->on('users');
             $table->foreign('red_id')->references('id')->on('redes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('redes_usuarios');
    }
}
