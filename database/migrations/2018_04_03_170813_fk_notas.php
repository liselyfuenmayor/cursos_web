<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FkNotas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


         Schema::table('notas',function (Blueprint $table) {

            $table->unsignedInteger('user_id');          
            $table->unsignedInteger('modulos_id'); 

             $table->foreign('modulos_id')->references('id')->on('modulos');
             $table->foreign('user_id')->references('id')->on('users');


            });
    

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notas', function (Blueprint $table) {
          
          $table->dropForeign('notas_modulos_id_foreign');
          $table->dropForeign('notas_user_id_foreign');
         

         });   
    }
}
