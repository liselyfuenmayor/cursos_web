<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumsUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users',function (Blueprint $table) {
         $table->unsignedInteger('roles_id');
         $table->string('lastname')->nullable();
         $table->string('ci');
         $table->string('phone')->nullable();
         $table->string('description')->nullable();
         $table->date('birth')->nullable();
         $table->integer('edad')->nullable();
         $table->boolean('gender')->nullable();

        $table->foreign('roles_id')->references('id')->on('roles');
    });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_roles_id_foreign');
            $table->dropColumn('roles_id');
            $table->dropColumn('lastname');
            $table->dropColumn('ci');
            $table->dropColumn('phone');
            $table->dropColumn('description');
            $table->dropColumn('birth');
            $table->dropColumn('edad');
            $table->dropColumn('gender');
    });
    }
}
