<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FkModulos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('modulos',function (Blueprint $table) {

        $table->unsignedInteger('curso_id');
        $table->unsignedInteger('user_id');

        $table->foreign('curso_id')->references('id')->on('cursos'); 
        $table->foreign('user_id')->references('id')->on('users');


    });

       

                }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('modulos', function (Blueprint $table) {
          
          $table->dropForeign('modulos_curso_id_foreign');
          $table->dropForeign('modulos_user_id_foreign');

         });
    }
}
