<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FkCertificados extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
           
     Schema::table('certificados',function (Blueprint $table) {

            $table->unsignedInteger('user_id'); 
            $table->unsignedInteger('curso_id');

            $table->foreign('curso_id')->references('id')->on('cursos'); 
            $table->foreign('user_id')->references('id')->on('users');


            });

                }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('certificados', function (Blueprint $table) {
          
          $table->dropForeign('certificados_curso_id_foreign');
          $table->dropForeign('certificados_user_id_foreign');
         

         }); 
    }
}
