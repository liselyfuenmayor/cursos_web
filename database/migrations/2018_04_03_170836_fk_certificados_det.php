<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FkCertificadosDet extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::table('certificado_dets',function (Blueprint $table) {

            $table->unsignedInteger('modulos_id');
            $table->unsignedInteger('certificado_id'); 
            $table->unsignedInteger('notas_id');
            
            $table->foreign('modulos_id')->references('id')->on('modulos');  
            $table->foreign('certificado_id')->references('id')->on('certificados');  
            $table->foreign('notas_id')->references('id')->on('notas'); 


            });

                }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('certificado_dets', function (Blueprint $table) {
          
          $table->dropForeign('certificado_dets_modulos_id_foreign');
          $table->dropForeign('certificado_dets_certificado_id_foreign');
          $table->dropForeign('certificado_dets_notas_id_foreign');
         

         });     }
}
