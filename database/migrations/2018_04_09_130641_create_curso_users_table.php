<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCursoUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('curso_users', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('curso_id');
            $table->date('fechainscri');
            $table->date('fechacerti')->nullable();
            $table->boolean('aceptado')->default(1);
            $table->integer('porcenculmin')->default(0);
            $table->integer('notafinal')->default(0);
            $table->timestamps();


             $table->foreign('user_id')->references('id')->on('users');
             $table->foreign('curso_id')->references('id')->on('cursos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('curso_users');
    }
}
