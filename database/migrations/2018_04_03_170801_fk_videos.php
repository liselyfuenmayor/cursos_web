<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FkVideos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('videos',function (Blueprint $table) {

                $table->unsignedInteger('modulos_id'); 

                $table->foreign('modulos_id')->references('id')->on('modulos');


            });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::table('videos', function (Blueprint $table) {
          
          $table->dropForeign('videos_modulos_id_foreign');
         

         });   

    }
}
