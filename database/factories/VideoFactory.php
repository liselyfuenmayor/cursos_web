<?php

use Faker\Generator as Faker;
use app\Videos;

$factory->define(App\Videos::class, function (Faker $faker) {
    return [

        'titulo' => substr($faker->sentence(3 ), 0, -1),
        'descripcion' => $faker->sentence(10),
        'urlVideo' => $faker->imageUrl(250, 250),      
        'modulos_id' => $faker->numberBetween(1, 75) 
        
    ];
});
