<?php

use Faker\Generator as Faker;
use app\Category; 

$factory->define(App\Category::class, function (Faker $faker) {
    return [

        'titulo' => ucfirst($faker->word),
        'descripcion' => $faker->sentence(10),
        'urlImg' => $faker->imageUrl(250, 250)
    ];
});
