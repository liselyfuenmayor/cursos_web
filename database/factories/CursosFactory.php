<?php

use Faker\Generator as Faker;
use app\Curso;

$factory->define(App\Curso::class, function (Faker $faker) {
    return [
        'titulo' => substr($faker->sentence(3 ), 0, -1),
        'descripcion' => $faker->sentence(10),
        'urlImg' => $faker->imageUrl(250, 250),      
        'category_id' => $faker->numberBetween(1, 5), 
        'user_id' => $faker->numberBetween(1, 5)   
    ];
});
