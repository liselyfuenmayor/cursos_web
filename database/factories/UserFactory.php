<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->firstName,
        'lastname' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'password' => $faker->numberBetween(1, 5000000) ,
        'remember_token' => str_random(10),
        'roles_id' => $faker->numberBetween(1, 5) ,
        'ci' => $faker->numberBetween(1, 50000000) ,
        'phone' => $faker->numberBetween(1, 50000000),
      	'description' => $faker->sentence(6),
      	'birth' => $faker->date('Y-m-d'),
        'edad' => $faker->numberBetween(1, 150) ,
        'gender' => $faker->numberBetween(0, 1)
       
         
    ];
});
