<?php

use Faker\Generator as Faker;
use app\Modulos;

$factory->define(App\Modulos::class, function (Faker $faker) {
    return [
        'titulo' => substr($faker->sentence(3 ), 0, -1),
        'descripcion' => $faker->sentence(10),    
        'curso_id' => $faker->numberBetween(1, 25), 
        'user_id' => $faker->numberBetween(1, 5)       ];
});
