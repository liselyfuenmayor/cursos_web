<?php

use Faker\Generator as Faker;

$factory->define(App\CursoUser::class, function (Faker $faker) {
    return [

        'fechainscri' => $faker->date('Y-m-d'),
        'fechacerti' => $faker->date('Y-m-d'),
        'user_id' => $faker->numberBetween(1, 5), 
        'curso_id' => $faker->numberBetween(1, 20),   
        'porcenculmin'=> $faker->numberBetween(1, 100),
        'notafinal'=> $faker->numberBetween(1, 20)
    ];
});
